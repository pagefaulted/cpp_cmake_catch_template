#include "catch.hpp"

int subtract(int x, int y) {
    return x - y;
}

TEST_CASE( "Subtract should subtract", "[subtract]" ) {
    REQUIRE( subtract(2, 1) == 1 );
    REQUIRE( subtract(2, 2) == 0 );
    REQUIRE( subtract(9, 3) == 6 );
    REQUIRE( subtract(10, 11) == -1 );
}

