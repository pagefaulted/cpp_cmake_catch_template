#include "catch.hpp"

unsigned int multiply( int n, int m ) {
    return n * m;
}

TEST_CASE( "Multiply should multiply", "[multiply]" ) {
    REQUIRE( multiply(1, 1) == 1 );
    REQUIRE( multiply(2, 1) == 2 );
    REQUIRE( multiply(3, 3) == 9 );
    REQUIRE( multiply(10, 100) == 1000 );
}

