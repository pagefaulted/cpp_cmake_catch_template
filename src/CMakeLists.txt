add_executable (main main.cpp)

# Add library. It helps with modularity and testing
#add_library (lib_name sources.cpp)
#target_link_libraries (main lib_name)
#target_sources(lib_name
#    PRIVATE
#        "${CMAKE_CURRENT_SOURCE_DIR}/file.cpp"
#        "${CMAKE_CURRENT_SOURCE_DIR}/other_file.cpp"
#)

