# C++ template project

This is a template project for C++.

The build tool is _CMake_.
The unit test library is [Catch2](https://github.com/catchorg/Catch2).

## Example commands

These commands will build the project as well as the tests and execute them:

```
mkdir build
cd build
cmake ../
cmake --build .
ctest .
```
This is the output:

```
Test project /home/mods/development/cpp_catch_based_template/build
    Start 1: some_module: :Multiply should multiply
1/3 Test #1: some_module: :Multiply should multiply ....   Passed    0.00 sec
    Start 2: some_module: :Subtract should subtract
2/3 Test #2: some_module: :Subtract should subtract ....   Passed    0.00 sec
    Start 3: some_other_module: :Factorials are cool
3/3 Test #3: some_other_module: :Factorials are cool ...   Passed    0.00 sec

100% tests passed, 0 tests failed out of 3

Label Time Summary:
factorial            =   0.00 sec*proc (1 test)
multiply             =   0.00 sec*proc (1 test)
some_module          =   0.01 sec*proc (2 tests)
some_other_module    =   0.00 sec*proc (1 test)
subtract             =   0.00 sec*proc (1 test)

Total Test time (real) =   0.01 sec
```
